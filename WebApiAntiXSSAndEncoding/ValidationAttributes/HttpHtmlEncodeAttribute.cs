﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace WebApiAntiXSSAndEncoding.ValidationAttributes
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class HttpHtmlEncodeAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if ((value == null) || !(value is string))
            {
                return null;
            }

            var stringVal = value as string;

            HtmlEncodeValue(stringVal, validationContext);
            
            return null;
        }

        private static void HtmlEncodeValue(string value, ValidationContext validationContext)
        {
            var encodedVal = HttpUtility.HtmlEncode(value);

            if (value.Equals(encodedVal))
            {
                return;
            }

            try
            {
                validationContext
                    .ObjectType
                    .GetProperty(GetMemberName(validationContext))
                    .SetValue(validationContext.ObjectInstance, encodedVal, null);
            }
            catch
            {
                // do nothing
            }
        }

        private static string GetMemberName(ValidationContext validationContext)
        {
            return string.IsNullOrEmpty(validationContext.MemberName) ? validationContext.DisplayName : validationContext.MemberName;
        }
    }
}