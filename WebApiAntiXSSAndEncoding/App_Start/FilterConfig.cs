﻿using System.Web.Http.Filters;
using System.Web.Mvc;
using WebApiAntiXSSAndEncoding.Filters;

namespace WebApiAntiXSSAndEncoding
{
    public class FilterConfig
    {
        public static void RegisterMvcGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }

        public static void RegisterHttpGlobaFilters(HttpFilterCollection filters)
        {
            filters.Add(new HttpValidateModelAttribute());
        }
    }
}