﻿using System.Net;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace WebApiAntiXSSAndEncoding.Filters
{
    public class HttpValidateModelAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            if (actionContext.ModelState.IsValid)
            {
                return;
            }
            
            actionContext.Response = actionContext.Request.CreateErrorResponse(HttpStatusCode.BadRequest, 
                actionContext.ModelState);
        }
    }
}