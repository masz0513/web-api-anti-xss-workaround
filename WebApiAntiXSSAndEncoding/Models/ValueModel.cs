﻿using System.ComponentModel.DataAnnotations;
using WebApiAntiXSSAndEncoding.ValidationAttributes;

namespace WebApiAntiXSSAndEncoding.Models
{
    public class ValueModel
    {
        [HttpHtmlEncode]
        [Required]
        public string Value { get; set; }

        [HttpHtmlEncode]
        public string Value2 { get; set; }
    }
}