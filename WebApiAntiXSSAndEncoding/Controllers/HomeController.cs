﻿using System.Web.Mvc;

namespace WebApiAntiXSSAndEncoding.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}
